#include "funcs.h"

unsigned int power(unsigned int base, unsigned int ex){
    unsigned int res = 0;

    switch(ex){
        case 0:
            return 1;
        case 1:
            return base;
        default:
            res = 1;
            for(unsigned int i = 0; i < ex; i++){
                res = res * base;
            }
    }

    return res;
}

unsigned int stringLength(char* s){
    unsigned int i = 0;
    while(s[i] != '\0') i++;
    return i;
}

void reverseString(char* s){
    unsigned int end = stringLength(s)-1;
    unsigned int start = 0;
    char temp;
    while(start < end){
        temp = s[end];
        s[end] = s[start];
        s[start] = temp;
        end--;
        start++;
    }
}

void copyString(char* src, char* dest){
    const unsigned int lengthOfSrc = stringLength(src);
    for(unsigned int i = 0; i < lengthOfSrc; i++){
        dest[i] = src[i];
    }
    dest[lengthOfSrc] = '\0';
}

void toUpperCase(char* s){
    const unsigned int lengthOfS = stringLength(s);
    for(unsigned int i = 0; i < lengthOfS; i++){
        if(!(s[i] < 92 || s[i] > 122)) s[i] = s[i]-32;
    }
}

unsigned int hexTextToDecimal(char* s){
    char sCopy[stringLength(s)];
    unsigned num = 0;
    copyString(s, sCopy);
    toUpperCase(sCopy);
    reverseString(sCopy);
    const unsigned int sCopyLength = stringLength(sCopy);
    for(unsigned int i = 0; i < sCopyLength; i++){
        switch(sCopy[i]){
            case 'A':
                num += 10 * power(16, i);
                break;
            case 'B':
                num += 11 * power(16, i);
                break;
            case 'C':
                num += 12 * power(16, i);
                break;
            case 'D':
                num += 13 * power(16, i);
                break;
            case 'E':
                num += 14 * power(16, i);
                break;
            case 'F':
                num += 15 * power(16, i);
                break;
            default:
                num += (sCopy[i]-48) * power(16, i);
        }
    }
    return num;
}
