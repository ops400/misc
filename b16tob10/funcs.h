#ifndef FUNCS_H_
#define FUNCS_H_

unsigned int power(unsigned int base, unsigned int ex);
unsigned int hexTextToDecimal(char* s);
unsigned int stringLength(char* s);
void reverseString(char* s);
void copyString(char* src, char* dest);
void toUpperCase(char* s);

#endif