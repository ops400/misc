#include "pico/stdlib.h"
#include "pico/stdio.h"
#include <stdio.h>
#include <stdint.h>

const uint8_t ledPin = 15;

int main(){
    stdio_init_all();
    gpio_init(ledPin);
    gpio_set_dir(ledPin, GPIO_OUT);
    char s[100];
    while(1){
        gpio_put(ledPin, 1);
        sleep_ms(250);
        gpio_put(ledPin, 0);
        sleep_ms(250);
        printf("Hello world!\n");
    }

    return 0;
}