#include "pico/stdlib.h"
#include "pico/stdio.h"
#include <stdint.h>
#include <stdio.h>

#define RS 7
#define RW 6
#define E  5
#define D0 4
#define D1 3
#define D2 2
#define D3 1
#define D4 0

const uint8_t pins[8] = {0, 1, 2, 3, 4, 5, 6, 7};

int main(){
    stdio_init_all();
    for(uint8_t i = 0; i < 8; i++){
        gpio_init(pins[i]);
        gpio_set_dir(pins[i], 0);
        gpio_pull_down(pins[i]);
    }
    while(1){
        for(uint8_t i = 0; i < 8; i++) printf("%u", gpio_get(pins[i]));
        printf("\n");
    }

    return 0;
}
