#include "pico/stdlib.h"
#include "pico/stdio.h"
#include <stdint.h>
#include <stdio.h>

#define RS 0
#define RW 1
#define E 2
#define D0 3
#define D1 4
#define D2 5
#define D3 6
#define D4 7
#define D5 8
#define D6 9
#define D7 10
//                       RS RW  E D0      ...           D7
const uint8_t pins[11] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

void pass(uint8_t bits[11]);

int main(){
    stdio_init_all();
    uint8_t bits[11];
    for(uint8_t i = 0; i < 11; i++){
        gpio_init(pins[i]);
        gpio_set_dir(pins[i], GPIO_OUT);
    }
    bits[RS] = 0;
    bits[RW] = 0;
    bits[E]  = 0;
    for(uint8_t i = 3; i < 11; i++) bits[i] = 0;

    bits[D0] = 1;
    bits[D1] = 1;
    bits[D2] = 1;
    bits[D3] = 1;

    pass(bits);
    gpio_put(pins[E], 1);
    gpio_put(pins[E], 0);

    return 0;
}

void pass(uint8_t bits[11]){
    for(uint8_t i = 0; i < 11; i++){
        gpio_put(pins[i], bits[i]);
        printf("%u", bits[i]);
    }
    printf("\n");
}