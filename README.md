# misc
The collection of miscellaneous projects of mine, but these ones are nothing big like [The Neco Arc Collection](https://gitlab.com/ops400/ncc), [Quite a Simple Editor](https://gitlab.com/ops400/qse) or [COMP24](https://gitlab.com/ops400/comp24).
