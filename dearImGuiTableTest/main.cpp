#include <cstdint>
#include <cstdio>
#include <cstring>
#include <raylib.h>
#include "deps/imgui.h"
#include "deps/rlImGui.h"

bool open = true;

typedef struct intVec2{
    int x, y;
} intVec2;

int main(int argc, char** argv){
    // bool openDemo = false;
    SetConfigFlags(FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT);
    InitWindow(640, 480, "Table Test");
    rlImGuiSetup(true);

    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
    io.MouseDrawCursor = true;

    const char* thingsForTable[2][5] = {
    {"Henrique", "10", "6.5", "7", "8"},
    {"Daniel", "0", "10", "9.6", "4"}};
    char buff[32] = "\0";
    char buff2[32] = "\0";
    uint8_t x = 0; // row
    uint8_t y = 0; // column
    
    bool dummy = true;
    ImGuiTableFlags tableFlags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders; 

    while(!WindowShouldClose()){
        BeginDrawing();
        ClearBackground(BLACK);
        
        rlImGuiBegin();
        ImGui::Begin("mainWindow", &open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize 
        | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoMove);
        
        ImGui::SetWindowSize(io.DisplaySize);
        ImGui::SetWindowPos((ImVec2){0, 0});
        ImGui::Text("%s", buff);
        ImGui::InputScalar("x", ImGuiDataType_U8, &x, &dummy);
        if(x > 1) x = 1;
        else x = x;
        ImGui::InputScalar("y", ImGuiDataType_U8, &y, &dummy);
        if(y > 4) y = 4;
        else y = y;
        if(ImGui::Button("Pass buffer")){
            strcpy(buff, thingsForTable[x][y]);
        }
        // if(ImGui::Button("Demo")) {openDemo = true; printf("HIII\n");}
        
        if(ImGui::BeginTable("tb1", 5, tableFlags)){
            ImGui::TableSetupColumn("Nome");
            ImGui::TableSetupColumn("1bi Nota");
            ImGui::TableSetupColumn("2bi Nota");
            ImGui::TableSetupColumn("3bi Nota");
            ImGui::TableSetupColumn("4bi Nota");
            ImGui::TableHeadersRow();
            for(uint8_t i = 0; i < 2; i++){
                ImGui::TableNextRow();
                for(uint8_t j = 0; j < 5; j++){
                    ImGui::TableSetColumnIndex(j);
                    strcpy(buff2, thingsForTable[i][j]);
                    ImGui::TextUnformatted(buff2);
                }

            }

            ImGui::EndTable();
        }

        ImGui::End();

        ImGui::ShowDemoWindow(&open);

        rlImGuiEnd();

        // DrawFPS(0, 0);
        EndDrawing();
    }

    rlImGuiShutdown();
    CloseWindow();
    return 0;
}