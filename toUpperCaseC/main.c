#include <stdio.h>
#include <stdint.h>

unsigned int lengthOfString(char* s);
void toUpperCase(char* s);

int main(int argc, char** argv){
        char* string = argv[1];
    switch(argc){
        case 1:
            printf("Not enough arguments\n");
            return 0;
        case 2:
            toUpperCase(string);
            printf("%s\n", string);
            break;
        default:
            printf("Argument error\n");
    }

    return 0;
}

unsigned int lengthOfString(char* s){
    unsigned int length = 0;
    while(s[length] != '\0') length++; 
    return length;
}

void toUpperCase(char* s){
    for(unsigned int i = 0; i < lengthOfString(s); i++){
        if(!(s[i] < 97 || s[i] > 122)) s[i] = s[i]-32;
    }
}