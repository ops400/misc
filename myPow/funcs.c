#include "funcs.h"

int power(int base, unsigned int ex){
    int res = 0;
    switch(ex){
        case 0:
            return 1;
        case 1:
            return base;
        default:
            res = 1;
            for(unsigned int i = 0; i < ex; i++){
                res = res * base;
            }
    }
    return res;
}

unsigned int lengthOfString(char* s){
    unsigned int length = 0;
    while(s[length] != '\0') length++;
    return length;
}

void reverseString(char* s){
    unsigned int start = 0;
    unsigned int end = lengthOfString(s)-1;
    char holder;
    while(start < end){
        holder = s[start];
        s[start] = s[end];
        s[end] = holder;
        start++;
        end--;
    }
}

void stringCopy(char* src, char* dest){
    for(unsigned int i = 0; i < lengthOfString(src); i++){
        dest[i] = src[i];
    }
    dest[lengthOfString(src)] = '\0';
}

int textToInt(char* s){
    char sCopy[lengthOfString(s)+1];
    stringCopy(s, sCopy);
    int num = 0;
    reverseString(sCopy);
    for(unsigned int i = 0; i < lengthOfString(sCopy); i++){
        switch(sCopy[i]){
            case '-':
                break;
            default:
                num += (sCopy[i]-48) * power(10, i);
        }
    }
    if(s[0] == '-') num = num * -1;
    return num;
}