#ifndef FUNCS_H_
#define FUNCS_H_

int power(int base, unsigned int ex);
unsigned int lengthOfString(char* s);
int textToInt(char* s);
void stringCopy(char* src, char* dest);
void reverseString(char* s);

#endif