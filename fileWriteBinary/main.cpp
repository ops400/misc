#include <cstdint>
#include <cstdio>

int main(void){
    const uint8_t values[2][5] = 
    {{0x15, 0x03, 0x00, 0x00, 0x00},{0x02, 0x03, 0x02, 0x00, 0x00}};
    FILE* fp;
    uint16_t test = 0xfc2a;
    printf("%x\n", test);
    uint8_t conv16[2] = {0x00, 0x00};
    uint16_t tempSeq = 0x0000;
    tempSeq = test << 8;
    conv16[0] = tempSeq >> 8;
    conv16[1] = test >> 8;
    for(uint8_t i = 0x00; i < 2; i++){
        printf("%x\n", conv16[i]);
    }
    uint32_t b24 = 0x0024fbd1;
    uint8_t conv[3] = {0x00, 0x00, 0x00};
    uint32_t temp24 = 0x00000000;
    temp24 = b24 << 24;
    conv[0] = temp24 >> 24;
    temp24 = b24 << 16;
    conv[1] = temp24 >> 24;
    conv[2] = b24 >> 16;
    printf("%x\n", b24);
    for(uint8_t i = 0; i < 3; i++){
        printf("%x\n", conv[i]);
    }
    const uint8_t values2[2][5] = 
    {{0x0b, 0x00, conv[0], conv[1], conv[2]}, {0x07, 0x05, conv16[0], conv16[1], 0x00}};
    fp = fopen("test.bin", "wb");
    fwrite(values[0], 1, 5, fp);
    fwrite(values[1], 1, 5, fp);
    fwrite(values2[0], 1, 5, fp);
    fwrite(values2[1], 1, 5, fp);
    fclose(fp);
    return 0;
}