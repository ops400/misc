const usrName = document.getElementById("usrFullName");
const ursBirthYear = document.getElementById("usrBirthYear");
const usrCpf = document.getElementById("usrCpf");
const resName = document.getElementById("resName");
const resYear = document.getElementById("resYear");
const resAge = document.getElementById("resAge");
const resCpf = document.getElementById("resCpf");

class user{
    constructor(name, yearOfBirth, cpf){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.cpf = cpf;
    }
    getName(){
        return this.name;
    }
    getYearOfBirth(){
        return this.yearOfBirth;
    }
    getCpf(){
        return this.cpf;
    }
    getAge(currentYear){
        return parseInt(currentYear - this.yearOfBirth);
    }
};

function passData(){
    let date = new Date;
    const usario = new user(usrName.value, parseInt(ursBirthYear.value), usrCpf.value);
    resName.textContent = usario.getName();
    resYear.textContent = usario.getYearOfBirth();
    resAge.textContent = usario.getAge(date.getFullYear());
    resCpf.textContent = usario.getCpf();
}
// good?
