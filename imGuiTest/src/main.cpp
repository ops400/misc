#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <ios>
#include <vector>
#include <raylib.h>
#include <cstdint>
#include <iostream>
#include "deps/imgui.h"
#include "deps/imgui_internal.h"
#include "deps/rlImGui.h"
#define WW 640
#define WH 480
#define WN "got"
#define BIT24_MAX 16777216

// const float spacingForSameLine = 1.0f;
// const float xOffsetForSameLine = 150.0f;

unsigned long int getFileSize(char filePath[500]);
void loadFile(char filePath[500], uint8_t* data, unsigned long int fileSize);

int main(int argc, char** argv){
    SetConfigFlags(FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE);
    InitWindow(WW, WH, WN);
    SetExitKey(KEY_NULL);
    rlImGuiSetup(true);

    ImGuiIO& io = ImGui::GetIO();
    (void)io;
    io.ConfigFlags|= ImGuiConfigFlags_NavEnableKeyboard;
    io.MouseDrawCursor = true;
    
    ImGuiWindowFlags mainWindowFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize;

    char filePath[500];
    static uint8_t dataArray[16777216];
    for(uint32_t i = 0; i < BIT24_MAX; i++) dataArray[i] = 0;
    bool openMemoryEditing = false;

    for(unsigned int i = 0; i < sizeof(filePath); i++) filePath[i] = 0;
    char sS[3][50];
    for(uint8_t i = 0; i < 3; i++){
        for(uint8_t j = 0; j < 50; j++){
            sS[i][j] = 0;
        }
    }
    // 0.19 ~= 2F
    const float bgColor = 0.13f;
    const ImVec4 disableText = {0.5f, 0.5f, 0.5f, 1.0f};
    const ImVec4 whitePure = {1.0f, 1.0f, 1.0f, 1.0f};
    const ImVec4 defaultFieldShit = {0.16f, 0.29f, 0.48f, 0.54f};
    const ImVec4 modifiedFieldShit = {0.16f, 0.29f, 0.48f, 0.0f};
    const ImVec4 customMenuBarGb = {0.204f, 0.204f, 0.204f, 1.0f};
    ImGuiStyle* style = &ImGui::GetStyle();
    style->Colors[ImGuiCol_WindowBg] = ImVec4{bgColor, bgColor, bgColor, 1.0f};
    style->Colors[ImGuiCol_MenuBarBg] = customMenuBarGb;

    uint32_t iStartDummy = 0x0;
    uint32_t iEndDummy = 0x55;

    uint32_t iStart = iStartDummy;
    uint32_t iEnd = iEndDummy;

    while(!WindowShouldClose()){
        BeginDrawing();
        ClearBackground(GRAY);
        rlImGuiBegin();
        
        ImGui::SetNextWindowSize(io.DisplaySize);
        ImGui::SetNextWindowPos(ImVec2{0.0f, 0.0f});
        ImGui::Begin("mainWindow", NULL, mainWindowFlags);
            ImGui::Text("%d", GetFPS());
            ImGui::SameLine();
            if(ImGui::BeginTabBar("tabBarMain")){
                if(ImGui::BeginTabItem("hello 1")){
                    ImGui::Text("Hello world");     
                    ImGui::InputText("File path", filePath, sizeof(filePath));       
                    if(ImGui::Button("Open file")){
                        std::cout << getFileSize(filePath) << "\n";
                        loadFile(filePath, dataArray, getFileSize(filePath));
                        std::cout << std::hex << (int)dataArray[5] << "\n"; 
                        openMemoryEditing = true;
                    }
                    ImGui::SameLine();
                    if(ImGui::Button("Open test3.bin")){
                        std::cout << getFileSize((char*)"test3.bin") << "\n";
                        loadFile((char*)"test3.bin", dataArray, getFileSize((char*)"test3.bin"));
                        std::cout << std::hex << (int)dataArray[5] << "\n"; 
                        openMemoryEditing = true;
                    }
                    if(openMemoryEditing){
                        ImGui::BeginGroup();
                        uint8_t biruleibe = 0;
                        int8_t biruleibe2 = 0;
                        bool iHaveAlreadyDrawnThisShit = false;

                        style->Colors[ImGuiCol_FrameBg] = modifiedFieldShit;
                        if(iStart > iEnd || iEnd > 0xFFFFFF){
                            iEnd = 0x55;
                            iStart = 0x0;
                        }
                        for(uint32_t i = iStart; i < iEnd; i++){
                                if(i%5 == 0){
                                    ImGui::Text("%06X:", i);
                                    ImGui::SameLine();
                                }
                                ImGui::SetNextItemWidth(20.0f);
                                if(dataArray[i] == 0) style->Colors[ImGuiCol_Text] = disableText;
                                else style->Colors[ImGuiCol_Text] = whitePure;
                                ImGui::InputScalar(TextFormat("##input%lX", i), ImGuiDataType_U8, &dataArray[i], NULL, NULL, "%02X");
                                if(biruleibe != 4){
                                    ImGui::SameLine();
                                    biruleibe++;
                                }
                                else{
                                    biruleibe = 0;
                                }
                            if(biruleibe2 == 4){
                                // printf("i:%u\nj:%d\n", i, i-4);
                                for(uint32_t j = i-4; j < i+1; j++){
                                    ImGui::SameLine();
                                    if(dataArray[j] > 32 && dataArray[j] < 127){
                                        style->Colors[ImGuiCol_Text] = whitePure;
                                        ImGui::Text("%c", dataArray[j]);
                                    }
                                    else{
                                        style->Colors[ImGuiCol_Text] = disableText;
                                        ImGui::TextUnformatted(".");
                                    }
                                }
                                biruleibe2 = -1;
                                // printf("DESENHAAAAAAAAAAAA PORRRRRRAAAAAAAA\n");
                            }
                            style->Colors[ImGuiCol_Text] = whitePure;
                            biruleibe2++;
                        }
                        style->Colors[ImGuiCol_FrameBg] = defaultFieldShit;
                        style->Colors[ImGuiCol_Text] = whitePure;
                        
                        ImGui::TextUnformatted("Memory access range:");
                        ImGui::SameLine();
                        ImGui::SetNextItemWidth(60.0f);
                        ImGui::InputScalar("##iStartDummySet", ImGuiDataType_U32, &iStartDummy, NULL, NULL, "%06X");
                        ImGui::SameLine();
                        ImGui::TextUnformatted("to");
                        ImGui::SameLine();
                        ImGui::SetNextItemWidth(60.0f);
                        ImGui::InputScalar("##iEndDummySet", ImGuiDataType_U32, &iEndDummy, NULL, NULL, "%06X");
                        if(ImGui::Button("Apply")){
                            iStart = iStartDummy;
                            iEnd = iEndDummy;
                        }
                        // if(iStart > iEnd || iEnd > 0xFFFFFF) wrongMemoryRangeModal = true;
                        ImGui::EndGroup();
                    }
                    ImGui::EndTabItem();
                }
                if(ImGui::BeginTabItem("hello 2")){
                    ImGui::Text("Hello to you");            
                    for(uint8_t i = 0; i < 3; i++){
                        ImGui::InputText(TextFormat("textInput%u", i), sS[i], sizeof(sS[i]));
                    }
                    for(uint8_t i = 0; i < 3; i++){
                        ImGui::Text("sS[%u]:%s", i, sS[i]);
                    }
                    ImGui::EndTabItem();
                }
                ImGui::EndTabBar();
            }
            ImGui::ShowDemoWindow((bool*)true);
        ImGui::End();

        rlImGuiEnd();
        EndDrawing();
    }

    rlImGuiShutdown();

    CloseWindow();
    return 0;
}

unsigned long int getFileSize(char filePath[500]){
    unsigned long int fileSize = 0;
    FILE *fp;
    fp = fopen(filePath, "rb");
    if(fp == NULL){
        std::cout << "oh shit\nError at file opening\n";
        exit(1);
    }
    fseek(fp, 0, SEEK_END);
    fileSize = ftell(fp);
    fclose(fp);
    return fileSize;
}

// void loadFile(char filePath[500], std::vector<uint8_t>* data, unsigned long int fileSize){
void loadFile(char filePath[500], uint8_t* data, unsigned long int fileSize){
    FILE *fp;
    fp = fopen(filePath, "rb");
    if(fp == NULL){
        std::cout << "oh shit\nError at file opening\n";
        exit(1);
    }
    uint8_t dataPrelude[fileSize];
    fread(dataPrelude, 1, fileSize, fp);
    std::cout << "got to loadFile()\n" << "dataPrelude[5]:" << (int)dataPrelude[5] << "\n";
    for(uint32_t i = 0; i < fileSize; i++){
        data[i] = dataPrelude[i];
    }
}
