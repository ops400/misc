#include <stdint.h>
#include <stdio.h>
#include <string.h>

uint8_t charToBool(char c);

int main(int argc, char** argv){
    if(argc > 1){
        unsigned int fNum = 0;
        uint8_t bitArray[strlen(argv[1])];
        for(unsigned int i = 0; i < sizeof(bitArray); i++) bitArray[i] = 0;
        for(unsigned int i = 0; i < strlen(argv[1]); i++){
            bitArray[i] += charToBool(argv[1][i]);
        }
        for(unsigned int i = 0; i < sizeof(bitArray); i++){
            fNum =  fNum << 1;
            fNum = fNum + bitArray[i];
        }
        printf("%d\n", fNum);
    }

    return 0;
}

uint8_t charToBool(char c){
    uint8_t num = 0;
    num = c - 48;
    return num;
}